# ABS Tutorial

[![pipeline status](https://gitlab.com/RSE-Lab-Fasilkom-UI/training2021/abs-tutorial/badges/master/pipeline.svg)](https://gitlab.com/RSE-Lab-Fasilkom-UI/training2021/abs-tutorial/-/commits/master)

## Getting Started

You need to download the ABS compiler (`absfrontend.jar`) and put it into
`bin/` directory. The latest version of ABS compiler tested for this tutorial
is [**v1.9.1**](https://github.com/abstools/abstools/releases/tag/v1.9.1).

Verify that the compiler can be loaded and run by Java:

```bash
$ java -jar ./bin/absfrontend.jar --version
ABS Tool Suite version v1.9.1
Built from git tree v1.9.1
```

If you want to build the compiler, please refer to the [project page on GitHub](https://github.com/abstools/abstools).
Besides Java, you will also need to have Erlang installed prior to building the
compiler.

## The Tutorial

Please follow the guide delivered via the static site of this project.
If you want to see the expected final result from doing this tutorial,
feel free to peruse the ABS files in [`src/reference`](./src/reference).
