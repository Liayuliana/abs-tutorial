# Getting Started

Before following through the tutorial, you will need to prepare the local
development environment. This tutorial requires Java and Eclipse IDE with
ABS plugin.

## Java

Prepare a working Java development environment by installing a Java SDK, e.g.,
[AdoptOpenJDK 11](https://adoptopenjdk.net/). Make sure Java-related toolchain
such as `javac` and `java` can be invoked via shell and `JAVA_HOME` environment
variable contains the path to the directory where Java SDK is installed:

```bash
$ env | grep openjdk
JAVA_HOME=/opt/java/openjdk
PATH=/opt/java/openjdk/bin:...(omitted for brevity)
$ javac
javac 11.0.9.1
$ java --version
openjdk 11.0.9.1 2020-11-04
OpenJDK Runtime Environment AdoptOpenJDK (build 11.0.9.1+1)
OpenJDK 64-Bit Server VM AdoptOpenJDK (build 11.0.9.1+1, mixed mode)
```

## Eclipse & ABS Plugin

Download and install [Eclipse Modeling Tools 2019-12 R][1]. Even though there
are newer Eclipse version, please do not try to install newer Eclipse because
the ABS plugin does not work on version greater than 2019-12.

Once you have downloaded and installed Eclipse Modeling Tools, open it and
install the ABS Plugin. The instructions to install the ABS plugin are as
follows:

1. Open _Help_ -> _Install New Software_ menu.
2. Click _Add_ button to add new source for installing Eclipse plugin.
3. Add `http://docs.abs-models.org/update-site` in the location input form
   and provide a descriptive name, e.g., `ABS`, in the name input form.
   Click _Add_ button to save the new plugin repository and return to the
   plugin installation window.

   ![New update site](images/setup-add-update-site.png)
4. Select the new ABS plugin repository and choose to install ABS. Install
   only the first three components of ABS plugin.

   ![Plugins to install](images/setup-add-plugin.png)
5. Click _Next_ button to proceed with the installation.
6. At the end of the installation process, you will be asked to restart the
   Eclipse. Please do as requested.
7. After restarting the Eclipse, verify that the plugin is working by
   switching perspective view to ABS. You can do so by opening _Window_ -> _Open Perspective_ -> _Other..._ -> _ABS_ menu.
   Alternatively, you can also check whether the Eclipse can create a new ABS
   project by right-click the _Project Explorer_ -> _New_ -> _Other..._ and see
   if the option to create ABS Project exists.

Once you have confirmed that the ABS plugin has been successfully installed,
proceed to the next tutorial instruction.

[1]: https://www.eclipse.org/downloads/packages/release/2019-12/r/eclipse-modeling-tools
