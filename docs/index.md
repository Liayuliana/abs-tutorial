# ABS Language Tutorial

![Header image](images/header.png)

Welcome to the [ABS (Abstract Behavioral Specification)][1] language tutorial.
This tutorial aims to provide a quick introduction to the ABS language. You
will learn how to write in ABS language and use its compiler to create an
executable model as program. Furthermore, you will also experience how ABS can
be used in SPLE (Software Product Line Engineering) activity.

If there are any questions regarding the tutorial, feel free to ask the
instructors during the live tutorial session or post the questions on the
[issue tracker][2].

## Getting Started

To complete the tutorial, you need to prepare your local development
environment. The following is the list of tools that will be used
throughout the tutorial:

- Java SDK
- ABS Compiler
- Your favorite text editor
- (optional) ABS syntax highlighter extension for Visual Studio Code
- (optional) Eclipse IDE & ABS plugin

Clone the Git repository into your local machine. The instructions
in this tutorial assume that you are working in a directory created
within the cloned repository.

```bash
$ pwd
/home/tutorial
$ git clone https://gitlab.com/RSE-Lab-Fasilkom-UI/training2021/abs-tutorial
$ cd abs-tutorial
$ pwd
/home/tutorial/abs-tutorial
```

The following subsections explain how to install and configure tools
that may require extra attention.

### Java

Prepare a working Java development environment by installing a Java SDK, e.g.,
[AdoptOpenJDK 11](https://adoptopenjdk.net/). Make sure Java-related toolchain
such as `javac` and `java` can be invoked via shell and `JAVA_HOME` environment
variable contains the path to the directory where Java SDK is installed:

```bash
$ env | grep openjdk
JAVA_HOME=/opt/java/openjdk
PATH=/opt/java/openjdk/bin:...(omitted for brevity)
$ javac
javac 11.0.9.1
$ java --version
openjdk 11.0.9.1 2020-11-04
OpenJDK Runtime Environment AdoptOpenJDK (build 11.0.9.1+1)
OpenJDK 64-Bit Server VM AdoptOpenJDK (build 11.0.9.1+1, mixed mode)
```

### ABS Compiler

Download the ABS compiler from [the official repository on GitHub](https://github.com/abstools/abstools)
into a directory called `bin/` in the cloned tutorial repository. Since we are
going to use the compiler, we only need to download the compiler executable,
i.e. `absfrontend.jar` via the releases page.

```bash
# Assuming you are currently in the root of cloned tutorial repository
$ mkdir -p bin
$ cd bin
$ wget https://github.com/abstools/abstools/releases/download/v1.9.1/absfrontend.jar
$ java -jar absfrontend.jar --version
ABS Tool Suite version v1.9.1
Built from git tree v1.9.1
```

> Note: To make ABS compiler invocation more concise later, you may also
> include the location of the compiler into `PATH` and `CLASSPATH` environment
> variables.

### Eclipse & ABS Plugin

> Note: The tutorial will be delivered mainly using CLI-based toolchain. You
> may install Eclipse and ABS plugin if you want to complete the tutorial using
> Eclipse IDE.

Download and install [Eclipse Modeling Tools 2019-12 R][5]. Even though there
are newer Eclipse version, please do not try to install newer Eclipse because
the ABS plugin does not work on version greater than 2019-12.

Once you have downloaded and installed Eclipse Modeling Tools, open it and
install the ABS Plugin. The instructions to install the ABS plugin are as
follows:

1. Open _Help_ -> _Install New Software_ menu.
2. Click _Add_ button to add new source for installing Eclipse plugin.
3. Add `http://docs.abs-models.org/update-site` in the location input form
   and provide a descriptive name, e.g., `ABS`, in the name input form.
   Click _Add_ button to save the new plugin repository and return to the
   plugin installation window.

   ![New update site](images/setup-add-update-site.png)
4. Select the new ABS plugin repository and choose to install ABS. Install
   only the first three components of ABS plugin.

   ![Plugins to install](images/setup-add-plugin.png)
5. Click _Next_ button to proceed with the installation.
6. At the end of the installation process, you will be asked to restart the
   Eclipse. Please do as requested.
7. After restarting the Eclipse, verify that the plugin is working by
   switching perspective view to ABS. You can do so by opening _Window_ -> _Open Perspective_ -> _Other..._ -> _ABS_ menu.
   Alternatively, you can also check whether the Eclipse can create a new ABS
   project by right-click the _Project Explorer_ -> _New_ -> _Other..._ and see
   if the option to create ABS Project exists.

## Next Instruction

Go to the next section, which is about creating a "Hello, World" executable
model.

## Credits & License

This document is licensed under [Creative Commmons Attribution Share Alike 4.0][3].
Some images are taken and/or remixed from [Irasutoya][4].

[1]: https://abs-models.org/
[2]: https://gitlab.com/RSE-Lab-Fasilkom-UI/training2021/abs-tutorial/-/issues
[3]: https://creativecommons.org/licenses/by-sa/4.0/
[4]: https://www.irasutoya.com
[5]: https://www.eclipse.org/downloads/packages/release/2019-12/r/eclipse-modeling-tools
