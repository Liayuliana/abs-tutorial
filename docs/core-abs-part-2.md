# Core ABS Part 2

## `CustomerIF` Module

Create an ABS module file named `Customer.abs` in the same directory as
`Account.abs` file.

```bash
$ pwd
/home/tutorial/abs-tutorial
$ touch src/main/Customer.abs
$ $EDITOR src/main/Customer.abs
```

Write the following code snippet into the module file:

```java
module CustomerIF;

export *;
import Level, createLevel from CustomerData;

interface Customer {
    Int getId();
    Customer findCustomer(CustomerList cl);
}

interface IndvCustomer extends Customer {

}

interface CorpCustomer extends Customer {
    Level getLevel();
}

type CustomerList = List<Customer>;

class CorpIndvCustomerImpl(Int id) implements IndvCustomer, CorpCustomer {
    Level lv = createLevel();

    Level getLevel() {
        return lv;
    }

    Int getId() {
        return id;
    }

    Customer findCustomer(CustomerList cl) {
        Customer result;
        Int i = 0;

        while (i < length(cl)) {
            Customer curCust = nth(cl, i);
            Int curId = curCust.getId();

            if (id == curId) {
                result = curCust;
            }

            i = i + 1;
        }

        return result;
    }
}

{
    Customer c = new local CorpIndvCustomerImpl(17);
    Customer d = new local CorpIndvCustomerImpl(16);
    Int n = c.getId();
    CustomerList l = Cons(c, Cons(d, Nil));
    Fut<Customer> e = c!findCustomer(l);
    await e?;
    Customer f = e.get;
}
```

As you can see in the code snippet, it is possible to have an ABS module whose
name is different from the file that contains it.

## `CustomerData` Module

Create an ABS module file named `CustomerData.abs` in the same directory as
`Account.abs` and `Customer.abs` files.

```bash
$ pwd
/home/tutorial/abs-tutorial
$ touch src/main/CustomerData.abs
$ $EDITOR src/main/CustomerData.abs
```

Write the following code snippet into the module file:

```java
module CustomerData;

export Level, Customer, createCustomer, createLevel, getPid;

data Level = Standard | Silver | Gold;
data Customer = Test | Person(Int pid, Level) | Company(Int taxId);

def Level createLevel() = Standard;

type CustomerList = List<Customer>;

def Int len(CustomerList list) =
    case list {
        Nil => 0;                       // data constructor pattern
        Cons(n, ls) => 1 + len(ls);     // data constructor pattern with pattern variable
        _ => 0;                         // underscore pattern (anonymous variable)
    };

def Int id(Customer c) =
    case c {
        Person(pid, _) => pid;
        Company(taxId) => taxId;
        _ => 0;
    };

def Int getPid(Customer c) = pid(c);

def Customer createCustomer(Int id, String kind) =
    case kind {
        "p" => Person(id, Standard);
        "c" => Company(id);
        _ => Test;
    };
```

We have completed all of the required modules. Now, try to compile and run
the main method from each ABS module. If you have installed Eclipse and ABS
plugin, you can also visualize interaction between COGs as sequence diagram.

## Next Instruction

The next instruction will cover the SPL-related features in ABS language.
