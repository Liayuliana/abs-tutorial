# Core ABS Part 1

## Module

Create a new ABS module named `Account.abs` and write the following code snippet
in the file:

```java
module Account;

export *;
import Customer, CorpIndvCustomerImpl from CustomerIF;
```

> Note: Do not worry if your tutorial example become not compilable after
> including the snippet above. Eventually, the pieces will be completed
> after following the remaining steps in this tutorial.

Intuitively, we can see that, conceptually, a module in ABS is similar to its
counterpart found in other programming languages such as Java (especially after
Java 9) and ECMAScript (modern JavaScript). A module is used to encapsulate
program elements (e.g., functions, classes) and determine which elements to
expose to/include from.

## Interface

The concept of _interface_ in ABS is similar to Java. It is used to declare
public methods that will be implemented by a class. The interface in ABS is
also extendable by other ABS interfaces.

The following code snippet is an example of ABS interface. Please write the
code snippet at the end of `Account.abs` (after `export` and `import`
statements).

```java
interface Account {
    Int getAid();
    Int deposit(Int x);
    Int withdraw(Int x);
}
```

## Class

The concept of _class_ in ABS is quite different from Java. First, ABS class
must implement at least one interface. Second, constructor method is replaced
with class parameters. Lastly, inheritance relationship is replaced by delta
modeling.

As usual, the following code snippet is an example of ABS class. Write the
code snippet into `Account.abs`.

```java
class AccountImpl(Int aid, Int balance, Customer owner) implements Account { // Core Product
    Int getAid() {
        return aid;
    }

    Int deposit(Int x) {
        balance = balance + x;
        return balance;
    }

    Int withdraw(Int y) {
        if (balance - y >= 0) {
            balance = balance - y;
        }

        return balance;
    }
}
```

## Attributes & Data Types

The following is some examples of frequently used data types in ABS:

- `Bool = True|False`
  Represents boolean data type.
- `Unit`
  Represents a data type that does not return any value. Similar to `void` type
  in Java.
- `Int`
  Represents integer data type.
- `Rat`
  Represents rational number data type.
- `String`
  Represents a sequence of symbols. Similar to `String` type in Java.
- `Fut<T>`
  Represents future data type. Cannot be constructed explicitly. It is used to
  receive returned value from asynchronous method calls. The actual value stored
  in the future data type can be obtained by using `get` expression.
- `List<A> = Nil | Cons(A, List<A>)`
  Represents a list data type. `Nil` indicates an empty list.

## Method

The method in ABS are still similar with Java. A method signature in ABS
comprises of name, parameter, and return type. What makes ABS method differ
from Java is the mechanism to invoke them. ABS has two ways of invoking method:
synchronous and asynchronous.

**Synchronous** method invocation in ABS is exactly the same as Java. That is,
by using dot notation. For example: `Bool foo = bar.isAlive();` On the other
hand, **asynchronous** method invocation in ABS replaces dot symbol with
exclamation mark (`!`). Therefore, the syntax is `<<objectIdentifier>>!<<methodName>>`.
For example: `Fut<Bool> foo2 = bar!isAlive();`

To obtain the actual value from a future variable, we use `get` expression
to block the current task until the actual value is available in the future
variable. While the task is blocked, tasks in other COG (Concurrent Object
Group) will also get blocked.

> Note: COG has been explained in the first day of the training. If you missed
> the explanation, you can think COG as similar to an actor or a thread in
> programming involving concurrency.

Additionally, we can also suspend the execution of ongoing task by using `await`
keyword. It will suspend the execution until the next guard expression evaluates
to boolean `True`. Example:

```java
Fut<Bool> f = x!m();
await f?;
await this.x == True;
await f? & this.y > 5;
```

Lastly, ABS does have main method to contain imperative and object-orientation
statements for instantiating and describing behavior of the model. The main
method is declared between open and close curly braces (i.e., `{}`). We can use
`new` statement to instantiate an object from a class with certain arguments.
The object can be instantiated into the same COG as current context or
different COG.

The following code snippet is an example of main method. Please also write it
into `Account.abs`.

```java
{
    Customer c = new local CorpIndvCustomerImpl(17);
    Account a = new AccountImpl(1,1,c);
    Fut<Int> dep = a!deposit(100);
    Fut<Int> withd = a!withdraw(10);
    await dep? & withd?;
    Int deposit = dep.get;
    Int withdraw = withd.get;
    Int net = deposit+withdraw;
}

```

## Comments

Writing comments in ABS is similar to Java and C. Example:

```java
// this is a comment
module A; // this is also a comment
/ *this
is a multiline
comment* /
```

## Next Instruction

Check your resulting `Account.abs` by comparing it to the reference at
[`src/reference/Account.abs`](../src/reference/Account.abs). We still
cannot compile and run the example because `Account.abs` require two
missing modules. We will create the required modules and its content
in the next instruction.
