# Software Product Line

In this section of the tutorial, you will try to create a feature model,
delta modules, product line configuration, and product specification as
part of realizing a simple SPL process.

## Feature Model

We can represent the features in our product line of bank account either
in visual or textual format. In ABS, we use uTVL notation to represent
the feature model in text-based format. For example, we have a visual
representation of the feature model of bank account as follows.

![Feature model of bank account](images/spl-feature-model.png)

Some explanation regarding the feature model notation:

- A feature box depicted with a circle on top means that the feature is an
  **optional** feature.
- A colored, rounded rectangle box beside a feature box indicates the
  **parameter** and **constraint** of the feature.
- The arc below a feature box that points to some features one level below
  indicates that only one feature may get chosen from the lower level.
  For example, we can only choose either `Check` or `Save`, but not both,
  when `Type` is included in our product.

The text-based version of the same feature model in ABS can be seen in the
following code snippet.

```json
root Account {
    group allof {
        Type {
            group oneof {
                Check {
                    ifin: Type.i == 0;
                },
                Save {
                    ifin: Type.i > 0;
                    exclude: Overdraft;
                }
            }
            Int i;
        },
        opt Fee {
            Int amount in [0..5];
        },
        opt Overdraft
    }
}
```

For now, we only defined the feature model. The feature model will not change
any behavior of our executable model when we rebuild and rerun the program.
The benefits will come up later when we have to define product variation.
We can avoid instantiating a product variant that violated the feature model.

## Delta Modeling

As explained in the first day of the training, product generation in ABS is
driven by applying delta modules to the core product. A core product will
be subsequently transformed by deltas required by one or more features in order
to create the desired product variant.

A delta in ABS can perform the following modification to a class or interface:

- Add and remove field(s)
- Add, remove, and modify method(s)
- Add a new interface to be implemented

The following subsections ask you to create delta modules. Make sure the files
containing the delta modules are placed in the same folder as previously
created ABS modules.

### Delta Module for `Fee` feature

Write the delta module in a file named `DFee.abs`. The content is as follows:

```java
delta DFee (Int fee);
uses Account;
modifies class AccountImpl {
    modifies Int deposit(Int x) {
        Int result = x;

        if (x >= fee) {
            result = original(x - fee);
        }

        return result;
    }
}

```

### Delta Module for `Save` Feature

Write the delta module in a file named `DSave.abs`. The content is as follows:

```java
delta DSave (Int i);
uses Account;
modifies class AccountImpl {
    removes Int interest;
    adds Int interest = i;
}
```

`DSave` delta modifies `interest` value at `AccountImpl` class. The delta
application process can be visualised in the following picture:

![A depiction of delta application process](images/spl-delta-application.png)

### Delta Module for `Type` Feature

Write the delta module in a file named `DType.abs`. The content is as follows:

```java
delta DType (Int i);
uses Account;
modifies class AccountImpl {
    adds Int interest = i;
}
```

### Delta Module for `Check` Feature

Write the delta module in a file named `DCheck.abs`. The content is as follows:

```java
delta DCheck;
uses Account;
modifies class AccountImpl {
    removes Int interest;
    adds Int interest = 0;
}
```

### Delta Module for `Overdraft` Feature

Write the delta module in a file named `DOverdraft.abs`. The content is as follows:

```java
delta DOverdraft;
uses Account;
modifies class AccountImpl {
    modifies Int withdraw(Int y) {
        balance = balance - y;
        return balance;
    }
}
```

## Product Line Configuration

Product line configuration describes the product line by specifying the features
present in the product line and defining the relationship between features and
deltas.

Write the product line configuration for the bank account product line in a file
named `AccountPL.abs`. The content is as follows:

```java
productline Accounts;
    features Fee, Overdraft, Check, Save, Type;
    delta DType(Type.i) when Type;
    delta DFee(Fee.amount) when Fee;
    delta DOverdraft after DCheck when Overdraft;
    delta DSave(Type.i) after DType when Save;
    delta DCheck after DType when Check;
```

Some important notes regarding the product line configuration:

- `when` clause is a **application condition** that associates a feature with
  a delta. Consequently, it determines which delta to apply when generating a
  product with specified feature set.
- A delta may have a parameter. The value will be determined by the product
  selection later.
- `after` clause determines the order of applying delta.

## Product Selection

Product selection defines the list of product and its associated features that
can be created in the product line. In this tutorial, we can have an example
of product selection as follows:

```java
// Basic product
product CheckingAccount (Type{i=0}, Check);

// Account with Fee and parameter
product AccountWithFee (Type{i=0}, Check, Fee{amount=1});

// Account with Overdraft
product AccountWithOverdraft (Type{i=0}, Check, Overdraft);

// Should be refused
// product SavingAccountWithOverdraft (Type{i=1}, Save, Overdraft);
```

Write the product selection into a file named `Products.abs` and put it in
the same folder as the other ABS files you have created.

## Generating a Product

For example, if you want to create `AccountWithOverdraft` product variant,
then execute the following statements in your shell:

```bash
java -jar .\bin\absfrontend.jar -j -p AccountWithOverdraft .\src\main
java -cp "bin/absfrontend.jar:gen" Account.Main
```

Notice the extra `-p` option when invoking the compiler. `-p` option specifies
the name of product to generate.

## End of Tutorial

That is all for today. Up until this point, you have tried almost all of the
ABS language features. If you are still curious with ABS, please refer to the
[official documentation of ABS language](https://abs-models.org/).
